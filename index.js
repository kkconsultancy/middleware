import Operations from './Operations'

class Middleware{
    state = {}
    
    constructor(callback){
        // console.log(callback)
        const operations = new Operations(this.state)
        this.state.flag = 0
 
        // console.log(middleware)
        operations.intialiseApp((res) => {

            this.login = operations.login
            this.logout = operations.logout
            this.getCustomToken = operations.getCustomToken
            this.customTokenLogin = operations.customTokenLogin 
            this.getForm = operations.getForm

            this.state.superCallback = callback

            this.reader = new FileReader()

            callback(res)
        })
    }

    fileReader = (file_blob,callback) => {
        this.reader.onload = function () {
            // console.log(this.reader.result);
            callback(this.reader.result)
        }.bind(this);
        this.reader.readAsText(file_blob);
    }

    convertCSVToJSON = (file_blob, keyValuePairs, callback) => {
        
        this.fileReader(file_blob, text => {
            // console.log(text.toString())
            let rows = text.split("\r").join("").split("\n")
            let header = rows[0].split(",")
            // console.log(header)
            rows = rows.splice(1)
            const json = rows.filter(row => row).map(row => {
                row = row.split(",")
                // console.log(row)
                let json = {}
                for (let i in header) {
                    // console.log(i,row[i].toString(),row[i] === "",!row[i])
                    if (row[i] && row[i] !== "")
                        json[keyValuePairs[0][header[i]]] = row[i]
                }
                return json
            })
            callback(json)
        })
    }

}

export default Middleware;