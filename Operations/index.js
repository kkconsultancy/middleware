import {auth,storage,initializeApp} from 'firebase'
import axios from 'axios'

import Firebase from '../Firebase'
class Operations{
    
    constructor(state){
        this.state = state
        // this.state.firebase = firebase()
        this.state.apiURL = window.location.hostname === "localhost" ? "http://localhost:5000/site-org-automation/us-central1" : "https://us-central1-site-org-automation.cloudfunctions.net"
        this.state.user = {}
        // console.log(this.state)

        this.getMetaData()
    }

    intialiseApp = (callback) => {
        axios.post(this.state.apiURL + '/getFirebaseConfig', { password: "siteautomation.developerswork.online" })
        .then( res => {
            if(res.data.code)
                throw new Error(res.data.code)
            // console.log(res)
            const json = res.data
            initializeApp(json)
            // this.state.firebase.initializeApp(json)
            let firebase = new Firebase(this.state)
            
            this.state.auth = auth()
            this.state.storage = storage().ref()
            
            this.login = firebase.login
            this.logout = firebase.logout
            this.customTokenLogin = firebase.customTokenLogin
            this.getCustomToken = firebase.getCustomToken

            firebase.authStateListener()
            callback({firebaseLoaded : 'LOADED'})
        })
        .catch(err => {
            console.log(err)
            callback(err)
        })
    }

    getMetaData = () => {
        axios.post(this.state.apiURL + '/api/v1/getMeta',{})
        .then(res => {
            this.state.metadata = {}
            if(!res.data.code){
                this.state.metadata = res.data ? res.data : {}
                const courses = []
                for(let course in res.data.courses){
                    courses.push(...res.data.courses[course])
                }
                // console.log(courses)
                this.state.metadata.courses = courses
            }            
            // console.log(this.state.metadata)
        })
        .catch(err => {
            console.log(err)
        })
    }

    getForm = (data,callback) => {
        const input = {
            version : data.version,
            category : data.category,
            type : data.type,
            uid : this.state.auth.currentUser.uid
        }

        return this.state.auth.currentUser.getIdToken()
        .then(token => {
            input.token = token
            return axios.post(this.state.apiURL + '/api/v1/' + this.state.auth.currentUser.uid + '/getForm', input)
        })
        .then(res => {
            if (!res.data.code) {
                return callback(res.data)
            }
        })
        .catch(err => {
            console.log(err)
        })
    }

    
}

export default Operations;