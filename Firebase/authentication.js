import axios from 'axios'


class Authentication{

    constructor(state){
        this.state = state
        this.state.JWT = {}
        // console.log(this.state)
    }

    getCustomToken = () => {
        return this.state.auth.currentUser.getIdToken().then(token => {
            console.log(token)
            this.state.JWT.token = token
            return axios.post(this.state.apiURL + '/api/v1/' + this.state.auth.currentUser.uid + '/getCustomToken', {
                uid: this.state.auth.currentUser.uid,
                token: token
            })
        }).then(res => {
            if(!res.data.code){
                // this.state.JWT.custom_token = res.data
                this.state.JWT.token = res.data.token
                return this.customTokenLogin(res.data)
            }
            console.log(res.data)
            throw new Error("FAILED")
        }).catch(err => {
            console.log(err)
            throw err
        })
    }

    login = (data, callback) => {
        return this.state.auth.signInWithEmailAndPassword(data.email, data.password)
        .then(user => {
            // console.log(this.state.auth)
            if(user)
                return this.getCustomToken()
            return callback({
                code : "auth/failed",
                message : "login failed"
            })
            
        }).catch(err => {
            return callback(err)
        })
    }

    requestResetPassword = (callback) => {
        this.state.auth.currentUser.getIdToken().then(token => {
            // console.log(callback)
            return axios.post(this.state.apiURL + '/api/v1/' + this.state.auth.currentUser.uid + '/requestResetPassword', {
                uid: this.state.auth.currentUser.uid,
                token: token
            })
        }).then(res => {
            if(res.data.code)
                callback({
                    code : "failed-reset-password",
                    "message" : "Unable to reset the password"
                })
            return callback(res.data)
        }).catch(err => {
            console.log(err)
            callback({
                code : "auth/failed"
            })
        })
    }

    customTokenLogin = (data, callback) => {
        return this.state.auth.signOut()
        .then(() => {
            return this.state.auth.signInWithCustomToken(data.token)
        }).then(res => {
            return res.user.getIdToken()
        }).then(token => {
            this.state.JWT.custom_token = token
            if(callback)
                return callback(token)
            return token
        }).catch(err => {
            throw err
        })
    }

    authStateListener = () => {
        return this.state.auth.onAuthStateChanged(user => {
            // console.log(user,new Date(),this.state)
            if (user){
                this.state.user = user
                if(!this.state.JWT.custom_token)
                    return this.getCustomToken(token => {
                        return this.state.JWT.custom_token = token
                    })
                else
                    return this.getServices((res) => {
                        this.state.authorisation = res
                        this.state.flag = 2
                        return this.state.superCallback({ authenticated: "DONE" })
                    })
            }
            else{
                if(this.state.flag === 0){
                    this.state.flag = 1
                    setTimeout(() => {
                        if(this.state.flag === 1)
                            return this.state.superCallback({ authenticated: "NOPE" })
                        return this.state.flag
                    },2000)
                }
                else{
                    this.state.user = {}
                    this.state.JWT = {}
                    this.state.authorisation = {}
                    this.state.superCallback({ authenticated: "NOPE" })
                }
            }
        })
    }

    logout = () => {
        return this.state.auth.signOut()
        .then(() => {
            this.state.user = {}
            this.state.JWT = {}
            this.state.authorisation = {}
            return this.state.superCallback({authenticated : "NOPE"})
        }).catch(err => {
            console.log(err)
            throw err
        })
    }

    getServices = (callback) => {
        if(!this.state.auth.currentUser)
            return callback({})
        this.state.auth.currentUser.getIdToken().then(token => {
            // console.log(callback)
            return axios.post(this.state.apiURL + '/api/v1/' + this.state.auth.currentUser.uid + '/getServices', {
                uid: this.state.auth.currentUser.uid,
                token: token
            })
        }).then(res => {
            if(res.data.code)
                return callback({
                    services : [],
                    roles : []
                })

            let services = {}
            res.data.services.filter(service => service.route).map(service => {
                const route = service.route.split("/")
                let previous = services
                let parent = services
                for(let i in route){
                    if(!parent[route[i]]){
                        parent[route[i]] = {}
                    }previous = parent
                    parent = parent[route[i]]
                }
                return previous[route.slice(-1)] = service
                // console.log(parent,previous)
            })
            services = {...services[""]}
            res.data.services = services
            // console.log(res.data)
            // console.log(services)
            return callback(res.data)
        }).catch(err => {
            console.log(err)
            // callback({
            //     code : "auth/failed"
            // })
            throw err
        })
    }


    getProfile = (data,callback) => {
        return this.state.auth.currentUser.getIdToken()
        .then(token => {
            return axios.post(this.state.apiURL + '/api/v1/' + this.state.auth.currentUser.uid + '/getUser', {
                uid: this.state.auth.currentUser.uid,
                token: token,
                username : this.state.auth.currentUser.uid
            })
        }).then(res => {
            return callback(res.data)
        }).catch(err => {
            console.log(err)
            return callback({
                "code": "incorrect-parameters",
                "message": "please provide valid input"
            })
        })
    }

}

export default Authentication