import axios from 'axios'

class Functions{

    state = {}
    constructor(state){
        this.state = state
    }

    createUsers = (users, callback) => {

        if(!Array.isArray(users))
            return callback({
                "code" : "incorrect-parameters",
                "message" : "please provide valid input"
            })

        return this.state.auth.currentUser.getIdToken()
        .then(token => {
            return axios.post(this.state.apiURL + '/api/v1/auth/' + this.state.auth.currentUser.uid + '/addUserAccounts', {
                uid: this.state.auth.currentUser.uid,
                token: token,
                users : users,
                timestamp : new Date().toUTCString()
            })
        }).then(res => {
            return callback(res.data)
        }).catch(err => {
            console.log(err)
            return callback({
                "code": "incorrect-parameters",
                "message": "please provide valid input"
            })
        })
    }

    listUsers = (data,callback) => {
        return this.state.auth.currentUser.getIdToken()
        .then(token => {
            return axios.post(this.state.apiURL + '/api/v1/auth/' + this.state.auth.currentUser.uid + '/listUsers', {
                ...data,
                uid: this.state.auth.currentUser.uid,
                token: token
            })
        }).then(res => {
            return callback(res.data)
        }).catch(err => {
            console.log(err)
            return callback({
                "code": "incorrect-parameters",
                "message": "please provide valid input"
            })
        })
    }

    listStudents = (data,callback) => {
        return this.state.auth.currentUser.getIdToken()
        .then(token => {
            return axios.post(this.state.apiURL + '/api/v1/auth/' + this.state.auth.currentUser.uid + '/listStudents', {
                uid: this.state.auth.currentUser.uid,
                token: token,
                ...data
            })
        }).then(res => {
            return callback(res.data)
        }).catch(err => {
            console.log(err)
            return callback({
                "code": "incorrect-parameters",
                "message": "please provide valid input"
            })
        })
    }

    postAttendance = (data,callback) => {
        return this.state.auth.currentUser.getIdToken()
        .then(token => {
            return axios.post(this.state.apiURL + '/api/v1/auth/' + this.state.auth.currentUser.uid + '/postAttendance', {
                uid: this.state.auth.currentUser.uid,
                token: token,
                ...data
            })
        }).then(res => {
            return callback(res.data)
        }).catch(err => {
            console.log(err)
            return callback({
                "code": "incorrect-parameters",
                "message": "please provide valid input"
            })
        })
    }

    viewAttendance = (data,callback) => {
        return this.state.auth.currentUser.getIdToken()
        .then(token => {
            return axios.post(this.state.apiURL + '/api/v1/auth/' + this.state.auth.currentUser.uid + '/viewAttendance', {
                uid: this.state.auth.currentUser.uid,
                token: token,
                ...data
            })
        }).then(res => {
            // console.log(res.data)
            return callback(res.data)
        }).catch(err => {
            console.log(err)
            return callback({
                "code": "incorrect-parameters",
                "message": "please provide valid input"
            })
        })
    }

    postMarks = (data,callback) => {
        return this.state.auth.currentUser.getIdToken()
        .then(token => {
            return axios.post(this.state.apiURL + '/api/v1/auth/' + this.state.auth.currentUser.uid + '/postMarks', {
                uid: this.state.auth.currentUser.uid,
                token: token,
                ...data
            })
        }).then(res => {
            return callback(res.data)
        }).catch(err => {
            console.log(err)
            return callback({
                "code": "incorrect-parameters",
                "message": "please provide valid input"
            })
        })
    }

    viewMarks = (data,callback) => {
        return this.state.auth.currentUser.getIdToken()
        .then(token => {
            return axios.post(this.state.apiURL + '/api/v1/auth/' + this.state.auth.currentUser.uid + '/viewMarks', {
                uid: this.state.auth.currentUser.uid,
                token: token,
                ...data
            })
        }).then(res => {
            return callback(res.data)
        }).catch(err => {
            console.log(err)
            return callback({
                "code": "incorrect-parameters",
                "message": "please provide valid input"
            })
        })
    }

    addStudentsToHostel = (data,callback) => {
        return this.state.auth.currentUser.getIdToken()
        .then(token => {
            return axios.post(this.state.apiURL + '/api/v1/auth/' + this.state.auth.currentUser.uid + '/addStudentsToHostel', {
                uid: this.state.auth.currentUser.uid,
                token: token,
                ...data
            })
        }).then(res => {
            return callback(res.data)
        }).catch(err => {
            console.log(err)
            return callback({
                "code": "incorrect-parameters",
                "message": "please provide valid input"
            })
        })
    }

    addStudentsToTransport = (data,callback) => {
        return this.state.auth.currentUser.getIdToken()
        .then(token => {
            return axios.post(this.state.apiURL + '/api/v1/auth/' + this.state.auth.currentUser.uid + '/addStudentsToTransport', {
                uid: this.state.auth.currentUser.uid,
                token: token,
                ...data
            })
        }).then(res => {
            return callback(res.data)
        }).catch(err => {
            console.log(err)
            return callback({
                "code": "incorrect-parameters",
                "message": "please provide valid input"
            })
        })
    }

    addSubjectsoAcademics = (data,callback) => {
        return this.state.auth.currentUser.getIdToken()
        .then(token => {
            return axios.post(this.state.apiURL + '/api/v1/auth/' + this.state.auth.currentUser.uid + '/addSubjectsoAcademics', {
                uid: this.state.auth.currentUser.uid,
                token: token,
                ...data
            })
        }).then(res => {
            return callback(res.data)
        }).catch(err => {
            console.log(err)
            return callback({
                "code": "incorrect-parameters",
                "message": "please provide valid input"
            })
        })
    }

    createPlacementPlan = (data,callback) => {
        return this.state.auth.currentUser.getIdToken()
        .then(token => {
            return axios.post(this.state.apiURL + '/api/v1/auth/' + this.state.auth.currentUser.uid + '/createPlacement', {
                uid: this.state.auth.currentUser.uid,
                token: token,
                ...data
            })
        }).then(res => {
            return callback(res.data)
        }).catch(err => {
            console.log(err)
            return callback({
                "code": "incorrect-parameters",
                "message": "please provide valid input"
            })
        })
    }

    hostelOutingGranted = (data,callback) => {
        return this.state.auth.currentUser.getIdToken()
        .then(token => {
            return axios.post(this.state.apiURL + '/api/v1/auth/' + this.state.auth.currentUser.uid + '/hostelOutingGranted', {
                uid: this.state.auth.currentUser.uid,
                token: token,
                ...data
            })
        }).then(res => {
            return callback(res.data)
        }).catch(err => {
            console.log(err)
            return callback({
                "code": "incorrect-parameters",
                "message": "please provide valid input"
            })
        })
    }


}

export default Functions