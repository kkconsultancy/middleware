
import axios from 'axios'

class FirebaseStorage{

    state = {}
    constructor(state){
        this.state = state
    
    }

    uploadPhotoURL = (data, callback) => {
        const extension = data.name.split(".").splice(-1)[0]

        const imageLocation = "UPLOADS/USERS/" + this.state.user.uid + "." + extension
        const ref = this.state.storage.child(imageLocation)
        // console.log(imageLocation)
        return ref.put(data)
            .then(() => {
                return this.state.auth.currentUser.getIdToken()
            }).then(token => {
                return axios.post(this.state.apiURL + '/api/v1/' + this.state.auth.currentUser.uid + '/updatePhotoURL', {
                    uid: this.state.auth.currentUser.uid,
                    token: token,
                    storageSourcePath: imageLocation,
                })
            }).then(res => {
                return callback(res.data)
            }).catch(err => {
                console.log(err)
            })
    }

}

export default FirebaseStorage