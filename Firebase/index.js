// import axios from 'axios'

import Storage from './storage'
import Functions from './functions'
import Authentication from './authentication'

// import test from './test.csv'

class Firebase{
    
    constructor(state){
        this.state = state
        this.state.middlewareAPI = {}

        const storage = new Storage(this.state)
        this.state.middlewareAPI = {
            ...this.state.middlewareAPI,
            uploadPhotoURL : storage.uploadPhotoURL
        }

        const functions = new Functions(this.state)
        this.state.middlewareAPI = {
            ...this.state.middlewareAPI,
            createUsers : functions.createUsers,
            listUsers : functions.listUsers,
            listStudents : functions.listStudents,
            postAttendance : functions.postAttendance,
            viewAttendance : functions.viewAttendance,
            postMarks : functions.postMarks,
            viewMarks : functions.viewMarks,
            addStudentsToHostel : functions.addStudentsToHostel,
            addStudentsToTransport : functions.addStudentsToTransport,
            addSubjectsoAcademics : functions.addSubjectsoAcademics
        }

        const authentication = new Authentication(this.state)
        this.state.middlewareAPI = {
            ...this.state.middlewareAPI,
            requestResetPassword : authentication.requestResetPassword,
            getProfile : authentication.getProfile
        }

        this.getCustomToken = authentication.getCustomToken
        this.login = authentication.login
        this.customTokenLogin = authentication.customTokenLogin
        this.authStateListener = authentication.authStateListener
        this.logout = authentication.logout

        // this.createUsers({name:"test.csv"},test)
        
    }
}

export default Firebase